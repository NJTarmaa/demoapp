const express = require('express')
const bodyParser = require('body-parser')
const fetch = require('node-fetch')
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express')
const mongoose = require('mongoose')
const myGraphQLSchema = require('./graphql/Schema')
const ObjectId = require('mongodb').ObjectID
const Rating = require('./models/rating')

const app = express()

// Enable mongo promises
mongoose.Promise = global.Promise

//require all models
// fs.readdirSync(__dirname + '/../models').forEach(function(filename)  {
//   if(~filename.indexOf('.js')) require( __dirname + './models/' + filename)
// })

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

// bodyParser is needed just for POST.
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema: myGraphQLSchema }))

// if you want GraphiQL enabled
app.get('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))

// Fetch locations from fake service
const getLocations = async () => {
  const koskiData = await fetch('http://fake-services:3000/fake-services/fishing/all-locations')
  return koskiData.json()
}

// respond with "hello world" when a GET request is made to the homepage
app.get('/', (req, res) => {
  res.json({ ok: 'ok' })
})

app.get('/locations', async (req, res) => {
  const locations = await getLocations()
  res.json(locations)
})

app.use('/rating', bodyParser.json())

app.post('/rating', (req, res) => {
  console.log('rating happened')
  mongoose.connect('mongodb://mongo/ratings', {
    useNewUrlParser: true,
  })

  let db = mongoose.connection

  let rating = new Rating({ rating: req.body.rating, uuid: req.body.id })
  rating.save((err) => {
    if(!err) {
      res.json({ ok: 200 })
      db.close()
    }
  })
})

app.get('/rating/:uuid', async (req, res) => {
  mongoose.connect('mongodb://mongo/ratings', {
    useNewUrlParser: true,
  })

  let db = mongoose.connection

  const rating = await Rating.aggregate(
    { $match : { uuid : { $regex: req.params.uuid } } },
    { $group:
      {
        _id: "$uuid",
        average: { $avg: "$rating" }
      }
    },
    { $project: { _id: 0, average: 1 } },
    (err, result) => {
      res.send(result)
      db.close()
    }
  )
})


app.listen(3000, () => console.log('Node server is listening on port 3000!'))

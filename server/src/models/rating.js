const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ratingSchema = new Schema({
  rating: Number,
  uuid: String
})

module.exports = mongoose.model('Rating', ratingSchema)

// Example model
// const Rating = mongoose.model('Rating', { rating: Number, uuid: String })

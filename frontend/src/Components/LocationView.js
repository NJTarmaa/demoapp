import React from 'react';
import PropTypes from 'prop-types';
import { Card, Icon, Button, Row, Col } from 'react-materialize';
import ReactStars from 'react-stars';

const LocationView = ({
    location,
    cleanState,
    ratingChanged,
    locationRating,
    locationImage,
    alertPrice,
}) => (
    <div>
        <Row>
            <Button id="back-button" onClick={cleanState}>
                <Icon>close</Icon>
            </Button>
        </Row>
        <Row>
            <Col s={12} className="location-name">
                {' '}
                <h5>{location.name}</h5>{' '}
            </Col>
        </Row>
        <Row className="location-prices-row">
            <Card>
                <div className="img-container">
                    {locationImage && (
                        <img
                            style={{
                                width: '40%',
                                padding: '10px',
                            }}
                            src={locationImage}
                            alt="gmaps"
                        />
                    )}
                </div>
                {/* Map with anonymous function, do not do these. Only for demonstration. */ location.prices.map(
                    price => (
                        <Row key={price.price} className="location-info">
                            <Col s={6}>
                                {price.price} - {price.type}
                            </Col>
                            <Button onClick={() => alert('Ostettu')}>
                                Osta
                            </Button>
                        </Row>
                    )
                )}
                <ReactStars
                    count={5}
                    value={locationRating}
                    onChange={ratingChanged}
                    size={24}
                    color2={'#ffd700'}
                    className="stars"
                />
            </Card>
        </Row>
    </div>
);

LocationView.defaultProps = {
    locationImage: null,
    number: 0,
};

LocationView.propTypes = {
    location: PropTypes.shape({
        address: PropTypes.string,
        gdata: PropTypes.shape({}),
        id: PropTypes.string,
        name: PropTypes.string,
        prices: PropTypes.arrayOf(PropTypes.shape),
        locationImage: PropTypes.string,
    }).isRequired,
    cleanState: PropTypes.func.isRequired,
    ratingChanged: PropTypes.func.isRequired,
    alertPrice: PropTypes.func.isRequired,
    locationImage: PropTypes.string,
    locationRating: PropTypes.number,
};

export default LocationView;

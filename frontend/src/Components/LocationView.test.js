import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import LocationView from './LocationView';

const mockCallback = jest.fn();

const location = {
    address: 'Huutokoski ',
    gdata: null,
    id: 'a07b780e-2941-4436-996b-9778fc9658b2',
    name: 'Huutokoski kalastusluvat 2018',
    prices: [],
    locationImage: null,
};

describe('Components -> <LocationView />', () => {
    it('shallow renders', () => {
        const wrapper = shallow(
            <LocationView
                location={location}
                cleanState={mockCallback}
                ratingChanged={mockCallback}
                alertPrice={mockCallback}
                locationImage={null}
                locationRating={null}
            />
        );
        expect(wrapper.find('Button#back-button')).to.have.length(1);
    });
});

import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import LocationRow from './LocationRow';

const mockCallback = jest.fn();

const location = {
    address: 'Huutokoski ',
    gdata: null,
    id: 'a07b780e-2941-4436-996b-9778fc9658b2',
    name: 'Huutokoski kalastusluvat 2018',
    prices: [],
    locationImage: null,
};

describe('Components -> <LocationRow />', () => {
    it('shallow renders', () => {
        const wrapper = shallow(
            <LocationRow showLocationView={mockCallback} location={location} />
        );
        expect(wrapper.find('h5')).to.have.length(1);
    });
});

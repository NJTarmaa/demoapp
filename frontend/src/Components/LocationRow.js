import React from 'react';
import PropTypes from 'prop-types';
import { Button, Row, Col } from 'react-materialize';

const LocationRow = ({ location, showLocationView }) => (
    <div>
        <Row className="location-row">
            <Col s={0} m={2} />
            <Col s={12} m={6}>
                {' '}
                <h5>{location.name} </h5>{' '}
            </Col>
            <Col s={2} />
            <Button
                onClick={() => {
                    showLocationView(location);
                }}
            >
                Lisätiedot
            </Button>
        </Row>
    </div>
);

Location.propTypes = {
    location: PropTypes.shape({}).isRequired,
    showLocationView: PropTypes.func.isRequired,
};

export default LocationRow;

import React, { Component } from 'react';
import styled from 'styled-components';
import './App.css';
import { Footer, Button, Icon, Row, Col, Input } from 'react-materialize';
import LocationRow from './Components/LocationRow';
import LocationView from './Components/LocationView';

// My awesome styled component
const Wrapper = styled.div``;

class App extends Component {
    constructor(props) {
        super(props);
        this.showLocationView = this.showLocationView.bind(this);
        this.state = {
            input: '',
            locations: [],
            location: this.props.location,
            locationView: false,
            locationRating: 0,
            locationImage: '',
        };
    }

    componentDidMount = async () => {
        await this.fetchAllLocations();
    };

    fetchAllLocations = async () => {
        const res = await fetch('http://localhost:4000/locations');
        const allLocations = await res.json();
        this.setState({
            locations: allLocations,
        });
    };

    fetchRatings = async id => {
        const res = await fetch(`http://localhost:4000/rating/${id}`);
        const rating = await res.json();
        if (!rating[0]) {
            return 0;
        }
        return rating[0].average;
    };

    clickHandler = () => {
        const { input } = this.state;
        console.log(input);
        // const { mutate } = this.props
        // mutate({
        //   variables: { locationTitle: input },
        // })
    };

    inputChange = e => {
        this.setState({
            input: e.target.value,
        });
    };

    showLocationView = async l => {
        const res = await this.fetchRatings(l.id);
        const rating = res;
        const locationImage = l.gdata.error_message
            ? null
            : `http://maps.googleapis.com/maps/api/staticmap?center=${
                  l.gdata.results[0].formatted_address
              }&zoom=10&size=300x200&markers=${
                  l.gdata.results[0].formatted_address
              }&sensor=false&key=${process.env.REACT_APP_GMAPS_ACCESS_KEY}`;
        this.setState({
            locationView: true,
            location: l,
            locationRating: Math.round(rating),
            locationImage: locationImage,
        });
    };

    cleanState = () => {
        this.setState({
            locationView: false,
            location: {},
            locationImage: '',
        });
    };

    ratingChanged = async newRating => {
        const { location } = this.state;
        let myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        const response = await fetch('http://localhost:4000/rating', {
            headers: myHeaders,
            method: 'POST',
            body: JSON.stringify({ rating: newRating, id: location.id }),
        });

        if (response.status >= 200 && response.status < 300) {
            return response;
        } else {
            let error = new Error(response.statusText);
            error.res = response;
            throw error;
        }
    };

    alertPrice = price => {
        alert('Hinta: ', price);
    };

    render() {
        const {
            locationView,
            input,
            locations,
            location,
            locationRating,
            locationImage,
        } = this.state;
        return (
            <div>
                {!locationView && (
                    <Wrapper>
                        <Row>
                            <Col className="header-text">
                                <h2> Kalamiehen Komrad </h2>
                            </Col>
                        </Row>
                        <div>
                            <Row className="search-row">
                                <Col s={0} m={4} />
                                <Col s={0} m={4}>
                                    <Input
                                        type="text"
                                        label="Location"
                                        value={input}
                                        onChange={this.inputChange}
                                        className="list-search"
                                    >
                                        <Icon>search</Icon>
                                    </Input>
                                </Col>
                                <Col s={0} m={2} className="search-button">
                                    <Button onClick={this.clickHandler}>
                                        Kuikka
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                        {/*TODO Don't do this! Map with anonymoys functions will eat your memory. */ locations.map(
                            l => (
                                <LocationRow
                                    key={l.id}
                                    location={l}
                                    showLocationView={this.showLocationView}
                                />
                            )
                        )}
                    </Wrapper>
                )}
                {locationView && (
                    <Row>
                        <LocationView
                            location={location}
                            cleanState={this.cleanState}
                            ratingChanged={this.ratingChanged}
                            locationRating={locationRating}
                            locationImage={locationImage}
                            alertPrice={this.alertPrice}
                        />
                    </Row>
                )}
                <Footer
                    copyrights="© 2019 Copyright"
                    className="app-footer"
                    links={
                        <ul>
                            <li>
                                <a
                                    className="grey-text text-lighten-3"
                                    href="https://gitlab.com"
                                >
                                    Gitlab (only for demo)
                                    <i
                                        className="fa fa-gitlab footer-icons gitlab-icon"
                                        aria-hidden="true"
                                        styles="color: orange;"
                                    />
                                </a>
                            </li>
                            <li>
                                <a
                                    className="grey-text text-lighten-3"
                                    href="https://facebook.com"
                                >
                                    Facebook (only for demo)
                                    <i
                                        className="fa fa-facebook footer-icons facebook-icon"
                                        aria-hidden="true"
                                        styles="color: orange;"
                                    />
                                </a>
                            </li>
                            <li>
                                <a
                                    className="grey-text text-lighten-3"
                                    href="https://instagram.com"
                                >
                                    Instagram (only for demo)
                                    <i
                                        className="fa fa-instagram footer-icons instagram-icon"
                                        aria-hidden="true"
                                        styles="color: orange;"
                                    />
                                </a>
                            </li>
                            <li>
                                <a
                                    className="grey-text text-lighten-3"
                                    href="https://twitter.com"
                                >
                                    Twitter (only for demo)
                                    <i
                                        className="fa fa-twitter footer-icons twitter-icon"
                                        aria-hidden="true"
                                        styles="color: orange;"
                                    />
                                </a>
                            </li>
                        </ul>
                    }
                >
                    <h5 className="white-text">Footer</h5>
                    <p className="grey-text text-lighten-4">
                        lorem ipsum dolor amet fashion axe palo santo leggings
                        tacos
                    </p>
                </Footer>
            </div>
        );
    }
}

// // GraphQL --- No time to do graphql
//
// // Query from server
// const query = gql`
//   query TodoAppQuery {
//     location {
//       title
//     }
//   }
// `
//
// // Call mutation
// const mutation = gql`
//   mutation fakeNews($locationTitle: String!) {
//     fakeNews(title: $locationTitle) {
//       title
//     }
//   }
// `
//
// // Update local cache
// const update = {
//   options: {
//     update: (proxy, { data: { fakeNews } }) => {
//       // Get current data
//       const data = proxy.readQuery({ query })
//
//       // write changes
//       proxy.writeQuery({
//         query,
//         data: {
//           ...data,
//           location: fakeNews,
//         },
//       })
//     },
//   },
// }

//TODO Remove compose and graphql wrappers --- No time to do graphql
// export default compose(graphql(query), graphql(mutation, update))(App)
export default App;

# Kalamiehen Komrad
Ultimate fishing location demo app
## Starting the app up

```
  docker-compose up
```

http://localhost:3000

## Checking running containers

```
  docker ps
```

## Stopping docker

```
  docker-compose down
```

## Cleanup docker
```
  docker container prune -f docker volume prune -f docker image prune -a
```

## Run test

To run the test you can do it with two options

- Run the tests with
``` npm test ```
- Or run tests with watch mode on
``` npm run test:watch ```
